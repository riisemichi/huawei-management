#Huawei Mobile Modem Configuration Wrapper for Python
Tested on Huawei E5372

## Installation
* Copy the file **huawei.config.template** and call it **huawei.config**
* Set username, password and log path

```
[webinterface]
url = http://192.168.8.1/html/home.html

[credentials]
username = admin
password = admin

[utility]
usagelog = /tmp
debug = false
```

If you set debug to true, the web configuration is started with chrome, else with phantomjs.

## Usage
### Script getuseddata.py 
This script will just return the used data found on the huawei modem and log it to the logfile specified in the config.

#### In Crontab
```
PATH=/usr/sbin:/usr/bin:/sbin:/bin:/usr/local/bin
0 0 * * * cd <Path to folder> && ./getuseddata.py > currentdata.txt
```

### Usage of Class HuaweiManagement in python
```
from huaweimanagement import HuaweiManagementSite

site = HuaweiManagementSite()

# load
site.load()

# log in
site.log_in()

# get used data
used = site.get_used_data()

# get device info
info = site.get_device_info()

# log out
site.log_out()

# close
site.close()
```
