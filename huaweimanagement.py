from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException, TimeoutException
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.ui import WebDriverWait
from huaweiconfig import HuaweiConfig


class Sms:
    id = ""
    sender = ""
    content = ""
    date = ""
    def __init__(self, id, sender, content, date):
        self.id = id
        self.sender = sender
        self.content = content
        self.date = date


class DeviceInfo:
    name = ""
    imei = ""
    imsi = ""
    phone_number = ""
    hw_version = ""
    sw_version = ""
    web_ui_version = ""
    mac = ""
    wan_ip = ""


class HuaweiManagementSite:
    _isloaded = False

    def __init__(self, config: HuaweiConfig = HuaweiConfig()):
        self.config = config
        if config.debug:
            self.driver = webdriver.Chrome()
        else:
            self.driver = webdriver.PhantomJS()

    def load(self):
        """
        Load Page.
        """
        self.driver.get(self.config.url)
        self._isloaded = True

    def close(self):
        """
        Close Page.
        """
        self.driver.close()
        self._isloaded = False

    def is_logged_in(self) -> bool:
        """
        Check for being logged in.
        :return: True = Logged in, False = not logged in.
        """
        WebDriverWait(self.driver, 10).until(
            expected_conditions.presence_of_element_located((By.ID, 'logout_span'))
        )
        return self.driver.find_element_by_id("logout_span").text == "Log Out"

    def log_in(self) -> bool:
        """
        Log in the the page using the credentials given in the config.
        :return: successful?
        """
        if self.is_logged_in():
            return True

        self.driver.find_element_by_id("logout_span").click()
        WebDriverWait(self.driver, 10).until(
            expected_conditions.presence_of_element_located((By.ID, 'username'))
        )
        try:
            self.driver.find_element_by_id("username").send_keys(self.config.username)
            self.driver.find_element_by_id("password").send_keys(self.config.password)
            self.driver.find_element_by_xpath('//*[@id="pop_login"]/span/span/span/a').click()

            # See if login was wrong
            try:
                WebDriverWait(self.driver, 1).until(
                    expected_conditions.presence_of_element_located(
                        (By.XPATH, '//*[@id="login_password_wrapper"]/span/div'))
                )
                print(self.driver.find_element_by_xpath('//*[@id="login_password_wrapper"]/span/div/label').text)
                # press cancel
                self.driver.find_element_by_xpath('//*[@id="pop_Cancel"]/span/span/span/a').click()
                return False
            except TimeoutException:
                pass

            WebDriverWait(self.driver, 10).until(
                expected_conditions.text_to_be_present_in_element((By.ID, "logout_span"), "Log Out")
            )
        except NoSuchElementException:
            print("error logging in, probably already logged in.")
        except TimeoutException:
            print("unknown error during login")

        return self.is_logged_in()

    def log_out(self) -> bool:
        """
        Log out of the page.
        :return: successful?
        """
        if not self.is_logged_in():
            return True

        try:
            self.driver.find_element_by_id("logout_span").click()
            self.driver.find_element_by_xpath('//*[@id="pop_confirm"]/span/span/span/a').click()
            WebDriverWait(self.driver, 10).until(
                expected_conditions.text_to_be_present_in_element((By.ID, "logout_span"), "Log In")
            )
        except NoSuchElementException:
            print("error logging out, probably already logged out.")
        return not self.is_logged_in()

    def is_tab_open(self, tab_id) -> bool:
        return len(self.driver.find_elements_by_css_selector("#"+tab_id+ ".active")) > 0

    def open_tab(self, tab_id) -> bool:
        # check if tab is already open. if it is open the <a> element contains the class "active".
        if self.is_tab_open(tab_id):
            return True

        tab = self.driver.find_element_by_id(tab_id)

        tab.click()

        # Wait until Statistic Site is loaded
        WebDriverWait(self.driver, 10).until(
            expected_conditions.presence_of_element_located((By.CSS_SELECTOR, "#"+tab_id+ ".active"))
        )
        return self.is_tab_open(tab_id)


    def get_used_data(self) -> str:
        """
        Find out how much data has been used this month.
        :return: String telling how much data has been used.
        """
        self._makesure_logged_in()
        self.open_tab("statistic")

        # Get the text
        used = self.driver.find_element_by_id("month_used_value").text
        return used

    def get_device_info(self) -> DeviceInfo:
        """
        Get the device info of the Huawei Modem.
        :return:
        """
        self._makesure_logged_in()
        self.open_tab("settings")

        self.driver.find_element_by_id("system").click()
        self.driver.find_element_by_xpath('//*[@id="deviceinformation"]/a').click()

        WebDriverWait(self.driver, 10).until(
            expected_conditions.presence_of_element_located(
                (By.XPATH, '//*[@id="wrapper"]/div/div[2]/div[2]/div[2]/table/tbody/tr[1]/td[2]'))
        )

        info = DeviceInfo()
        info.name = self.driver.find_element_by_xpath(
            '//*[@id="wrapper"]/div/div[2]/div[2]/div[2]/table/tbody/tr[1]/td[2]').text
        info.imei = self.driver.find_element_by_xpath(
            '//*[@id="wrapper"]/div/div[2]/div[2]/div[2]/table/tbody/tr[2]/td[2]').text
        info.imsi = self.driver.find_element_by_xpath(
            '//*[@id="wrapper"]/div/div[2]/div[2]/div[2]/table/tbody/tr[3]/td[2]').text
        info.phone_number = self.driver.find_element_by_xpath(
            '//*[@id="wrapper"]/div/div[2]/div[2]/div[2]/table/tbody/tr[4]/td[2]').text
        info.hw_version = self.driver.find_element_by_xpath(
            '//*[@id="wrapper"]/div/div[2]/div[2]/div[2]/table/tbody/tr[5]/td[2]').text
        info.sw_version = self.driver.find_element_by_xpath(
            '//*[@id="wrapper"]/div/div[2]/div[2]/div[2]/table/tbody/tr[6]/td[2]').text
        info.web_ui_version = self.driver.find_element_by_xpath(
            '//*[@id="wrapper"]/div/div[2]/div[2]/div[2]/table/tbody/tr[7]/td[2]').text
        info.mac = self.driver.find_element_by_xpath(
            '//*[@id="wrapper"]/div/div[2]/div[2]/div[2]/table/tbody/tr[8]/td[2]').text
        info.wan_ip = self.driver.find_element_by_xpath(
            '//*[@id="wrapper"]/div/div[2]/div[2]/div[2]/table/tbody/tr[9]/td[2]').text

        return info

    def get_sms(self) -> [Sms]:
        self._makesure_logged_in()
        self.open_tab("sms")

        sms_rows = self.driver.find_elements_by_css_selector('#sms_table > tbody > tr.sms_list_tr')

        messages = []
        for row in sms_rows:
            id = str(row.find_element_by_css_selector('.sms_td input').get_attribute("value"))
            sender = row.find_element_by_css_selector('td.sms_phone_number').text
            content = row.find_element_by_css_selector('pre.sms_content').text
            date = row.find_element_by_css_selector('td.sms_date_width label').text
            messages.append(Sms(id, sender, content, date))
        return messages

    def delete_sms(self, messages: [Sms]):
        self._makesure_logged_in()
        self.open_tab("sms")

        for sms in messages:
            box = self.driver.find_element_by_xpath('//*[@id="sms_table"]/tbody/tr/td[1]/input[@value="'+sms.id+'"]')
            box.click()

        deletebutton = self.driver.find_element_by_xpath('//*[@id="del_msg_btn"]/span/span/span')
        deletebutton.click()

        WebDriverWait(self.driver, 10).until(
            expected_conditions.presence_of_element_located((By.XPATH, '//*[@id="pop_confirm"]/span/span/span/a'))
        )

        confirmbutton = self.driver.find_element_by_xpath('//*[@id="pop_confirm"]/span/span/span/a')
        confirmbutton.click()


    def _makesure_logged_in(self):
        if not self._isloaded:
            self.load()
        if not self.is_logged_in():
            self.log_in()


