import time
import datetime
from huaweimanagement import HuaweiManagementSite

def testGetData():
    site = HuaweiManagementSite()

    site.load()
    if not site.log_in():
        print("error logging in")
        pass

    usedData = site.get_used_data()

    timestamp = datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S')

    print(timestamp + ": " + usedData)

    site.log_out()
    site.close()


def testWrongPassword():
    site = HuaweiManagementSite()

    site.load()
    site.config.password = "asdlkfj"

    site.log_in()
    site.close()

def test_get_device_info():
    site = HuaweiManagementSite()

    site.load()
    site.log_in()

    info = site.get_device_info()

    site.log_out()
    site.close()


    attrs = vars(info)
    print(', '.join("%s: %s" % item for item in attrs.items()))

def test_get_sms():
    site = HuaweiManagementSite()

    site.load()
    site.log_in()

    sms = site.get_sms()

    for element in sms:
        print(element.id + " " + element.sender + ": " + element.content)

    #site.delete_sms([sms[3]])

    site.log_out()
    site.close()

# Do Stuff
#testWrongPassword()
#testGetData()
test_get_device_info()
#test_get_sms()