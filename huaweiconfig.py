import os
from configparser import ConfigParser


class HuaweiConfig:
    def __init__(self):
        if not self.readConfig(os.path.dirname(os.path.realpath(__file__))+'/huawei.config'):
            self.url = "http://192.168.8.1/html/home.html"
            self.username = "admin"
            self.password = "admin"
            self.usagelog = "/home/pi/tools/huawei/data.log"


    def readConfig(self, file) -> bool:
        if os.path.exists(file):
            config = ConfigParser()
            config.read(file)

            self.url = config['webinterface']['url']
            self.username = config['credentials']['username']
            self.password = config['credentials']['password']
            self.usagelog = config['utility']['usagelog']
            self.smslog = config['utility']['smslog']
            self.sms_forwarding_mail = config['utility']['sms_forwarding_mail']
            self.debug = self.str2bool(config['utility']['debug'])

            return True
        else:
            return False

    @staticmethod
    def str2bool(v):
        return v.lower() in ("yes", "true", "t", "1")