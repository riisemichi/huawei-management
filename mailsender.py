_version_ = "1.0.0"
import urllib.parse
import urllib.request


class Mail:
    def __init__(self, to, subject, text):
        self.to = to
        self.subject = subject
        self.text = text


class MailSender:
    def __init__(self, appname):
        self.appname = appname

    def send(self, mail):
        return self.__send(mail.to, mail.subject, mail.text)

    def __send(self, to, subject, text):
        url = 'http://appdata.niceapps.ch/mailsender/sendmailnow.php'

        user_agent = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.64 Safari/537.11'

        values = {'from' : self.appname + '@niceapps.ch',
                  'to' : to,
                  'subject' : subject,
                  'text' : text,
                  'pw' : '8C52AC0D-8339-4531@88B4.KrassMann101223448813'}
        headers = { 'User-Agent' : user_agent }

        data  = urllib.parse.urlencode(values)
        data = data.encode('utf-8')

        req = urllib.request.Request(url, data, headers)
        with urllib.request.urlopen(req) as response:
            content = str(response.read())

        return ("Nachricht erfolgreich versandt" in content)
