#! /usr/bin/python3

import time
import datetime
from huaweimanagement import HuaweiManagementSite, Sms


# Get used Data
from mailsender import MailSender, Mail


def log(text):
    with open(config.smslog, "a") as logfile:
        one_line_text = text.replace("\n", "")
        print(one_line_text)
        logfile.write(datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S: ') + one_line_text + "\n")

def log_sms(sms:[Sms]):
    log(sms.id+";"+sms.date+";"+sms.sender+";"+sms.content)

def sms_to_mail(sms:Sms):
    sender = MailSender("bandraum")
    mail = Mail(config.sms_forwarding_mail, "SMS von " + sms.sender, sms.content)
    sender.send(mail)


site = HuaweiManagementSite()
try:
    config = site.config

    site.load()
    if not site.log_in():
        log("error logging in")
        pass

    messages = site.get_sms()

    for sms in messages:
        log_sms(sms)
        sms_to_mail(sms)

    if len(messages) > 0:
        site.delete_sms(messages)
except:
    log("Error!")

site.log_out()
site.close()

