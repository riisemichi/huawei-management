#! /usr/bin/python3

import time
import datetime
from huaweimanagement import HuaweiManagementSite

# Get used Data
def log(text):
    with open(config.usagelog, "a") as logfile:
        print(text)
        logfile.write(datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S: ') + text + "\n")

site = HuaweiManagementSite()
config = site.config

site.load()
if not site.log_in():
    log("error logging in")
    pass

usedData = site.get_used_data()

site.log_out()
site.close()

log(usedData)
